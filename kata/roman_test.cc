#include "roman.h"
#include "gtest/gtest.h"

TEST(RomanTest, ShouldReturnI) { 
  EXPECT_EQ("I", Roman(1));
}

int main(int argc, char **argv) { 
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
