#include "fizzbuzz.h"
#include <string>

using namespace std;

string FizzBuzz(int n) {
  string result = to_string(n);
  if(n%3 == 0) {
    result = "Fizz";
    if(n%5 == 0) {
      result.append("Buzz");
    }
  } else if(n%5 == 0) { 
    result = "Buzz";
  }
  return result;
}
