#include "fizzbuzz.h"
#include "gtest/gtest.h"

TEST(FizzBuzzTest, ShouldReturnOne) { 
  EXPECT_EQ("1", FizzBuzz(1));
}

TEST(FizzBuzzTest, ShouldReturnTwo) { 
  EXPECT_EQ("2", FizzBuzz(2));
}

TEST(FizzBuzzTest, ShouldReturnFizz) { 
  EXPECT_EQ("Fizz", FizzBuzz(3));
  EXPECT_EQ("Fizz", FizzBuzz(6));
  EXPECT_EQ("Fizz", FizzBuzz(9));
}

TEST(FizzBuzzTest, ShouldReturnFour) { 
  EXPECT_EQ("4", FizzBuzz(4));
}

TEST(FizzBuzzTest, ShouldReturnBuzz) { 
  EXPECT_EQ("Buzz", FizzBuzz(5));
  EXPECT_EQ("Buzz", FizzBuzz(10));
}

TEST(FizzBuzzTest, ShouldReturnFizzBuzz) { 
  EXPECT_EQ("FizzBuzz", FizzBuzz(15));
  EXPECT_EQ("FizzBuzz", FizzBuzz(30));
}

int main(int argc, char **argv) { 
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
