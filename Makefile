GTEST_DIR = ./gtest-1.7.0
USER_DIR = ./kata

CPPFLAGS += -isystem $(GTEST_DIR)/include
CXXFLAGS += -g -Wall -Wextra -pthread

BUILD_DIR = ./builds
BIN_DIR = ./test-bin
TESTS = ${BIN_DIR}/fizzbuzz_test ${BIN_DIR}/roman_test

GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h

#
# Build targets.
#

all : clean create_dir $(TESTS) 

clean :
	rm -rf ${BUILD_DIR}
	rm -rf ${BIN_DIR}

create_dir :
	mkdir ${BUILD_DIR}
	mkdir ${BIN_DIR}

test :
	${BIN_DIR}/fizzbuzz_test
	${BIN_DIR}/roman_test

#
# Build GTest 
#

GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

${BUILD_DIR}/gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest-all.cc -o ${BUILD_DIR}/gtest-all.o

${BUILD_DIR}/gtest_main.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest_main.cc -o ${BUILD_DIR}/gtest_main.o 

${BUILD_DIR}/gtest.a : ${BUILD_DIR}/gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

${BUILD_DIR}/gtest_main.a : ${BUILD_DIR}/gtest-all.o ${BUILD_DIR}/gtest_main.o
	$(AR) $(ARFLAGS) $@ $^

# 
# Build kata sourcecode
#

# FizzBuzz

${BUILD_DIR}/fizzbuzz.o : $(USER_DIR)/fizzbuzz.cc $(USER_DIR)/fizzbuzz.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/fizzbuzz.cc -o ${BUILD_DIR}/fizzbuzz.o

${BUILD_DIR}/fizzbuzz_test.o : $(USER_DIR)/fizzbuzz_test.cc $(USER_DIR)/fizzbuzz.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/fizzbuzz_test.cc -o ${BUILD_DIR}/fizzbuzz_test.o

${BIN_DIR}/fizzbuzz_test : ${BUILD_DIR}/fizzbuzz.o ${BUILD_DIR}/fizzbuzz_test.o ${BUILD_DIR}/gtest_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -lpthread $^ -o $@

# Roman 

${BUILD_DIR}/roman.o : $(USER_DIR)/roman.cc $(USER_DIR)/roman.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/roman.cc -o ${BUILD_DIR}/roman.o

${BUILD_DIR}/roman_test.o : $(USER_DIR)/roman_test.cc $(USER_DIR)/roman.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/roman_test.cc -o ${BUILD_DIR}/roman_test.o

${BIN_DIR}/roman_test : ${BUILD_DIR}/roman.o ${BUILD_DIR}/roman_test.o ${BUILD_DIR}/gtest_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -lpthread $^ -o $@

